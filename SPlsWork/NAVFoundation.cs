using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_NAVFOUNDATION
{
    public class UserModuleClass_NAVFOUNDATION : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        static public void NAVLOG (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SPARAM ) 
            { 
            
            /*162*/ __caller__.Trace( "{0}", SPARAM ) ; 
            
            }
            
        static public CrestronString NAVTRACEHEX (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SPARAM ) 
            { 
            CrestronString STEMP;
            STEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1024, __caller__ );
            
            ushort X = 0;
            
            
            /*168*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( SPARAM ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( X  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (X  >= __FN_FORSTART_VAL__1) && (X  <= __FN_FOREND_VAL__1) ) : ( (X  <= __FN_FORSTART_VAL__1) && (X  >= __FN_FOREND_VAL__1) ) ; X  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*169*/ STEMP  .UpdateValue ( STEMP + "\u005C\u0078" + Functions.ItoHex (  (int) ( __caller__.Byte( SPARAM , (int)( X ) ) ) )  ) ; 
                /*168*/ } 
            
            /*172*/ return ( STEMP ) ; 
            
            }
            
        static public CrestronString NAVSTRIPCHARSFROMRIGHT (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SPARAM , ushort IPARAM ) 
            { 
            CrestronString STEMP;
            STEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1024, __caller__ );
            
            
            /*177*/ STEMP  .UpdateValue ( SPARAM  ) ; 
            /*178*/ return ( Functions.Left ( STEMP ,  (int) ( (Functions.Length( STEMP ) - IPARAM) ) ) ) ; 
            
            }
            
        static public CrestronString NAVSTRIPCHARSFROMLEFT (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SPARAM , ushort IPARAM ) 
            { 
            
            /*182*/ return ( Functions.Right ( SPARAM ,  (int) ( (Functions.Length( SPARAM ) - IPARAM) ) ) ) ; 
            
            }
            
        static public ushort NAVCONTAINS (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SPARAM , CrestronString SMATCH ) 
            { 
            
            /*186*/ return (ushort)( Functions.Find( SMATCH , SPARAM )) ; 
            
            }
            
        static public ushort NAVSTARTSWITH (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SPARAM , CrestronString SMATCH ) 
            { 
            
            /*190*/ return (ushort)( Functions.BoolToInt (NAVCONTAINS( __context__ , __caller__ , SPARAM , SMATCH ) == 1)) ; 
            
            }
            
        static public ushort NAVENDSWITH (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SPARAM , CrestronString SMATCH ) 
            { 
            
            /*194*/ return (ushort)( Functions.BoolToInt (Functions.Right( SPARAM , (int)( Functions.Length( SMATCH ) ) ) == SMATCH)) ; 
            
            }
            
        static public ushort NAVCALCULATESUMOFBYTESCHECKSUM (  SplusExecutionContext __context__, SplusObject __caller__ , ushort ISTARTBYTE , CrestronString SPARAM ) 
            { 
            ushort X = 0;
            ushort ICHK = 0;
            
            
            /*200*/ ICHK = (ushort) ( 0 ) ; 
            /*201*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( ISTARTBYTE ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( SPARAM ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( X  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (X  >= __FN_FORSTART_VAL__1) && (X  <= __FN_FOREND_VAL__1) ) : ( (X  <= __FN_FORSTART_VAL__1) && (X  >= __FN_FOREND_VAL__1) ) ; X  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*202*/ ICHK = (ushort) ( (ICHK + __caller__.Byte( SPARAM , (int)( X ) )) ) ; 
                /*201*/ } 
            
            /*205*/ return (ushort)( Functions.Low( (ushort) ICHK )) ; 
            
            }
            
        static public ushort NAVCALCULATEXOROFBYTESCHECKSUM (  SplusExecutionContext __context__, SplusObject __caller__ , ushort ISTARTBYTE , CrestronString SPARAM ) 
            { 
            ushort X = 0;
            ushort ICHK = 0;
            
            
            /*210*/ ICHK = (ushort) ( 0 ) ; 
            /*211*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( ISTARTBYTE ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( SPARAM ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( X  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (X  >= __FN_FORSTART_VAL__1) && (X  <= __FN_FOREND_VAL__1) ) : ( (X  <= __FN_FORSTART_VAL__1) && (X  >= __FN_FOREND_VAL__1) ) ; X  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*212*/ ICHK = (ushort) ( (ICHK ^ __caller__.Byte( SPARAM , (int)( X ) )) ) ; 
                /*211*/ } 
            
            /*215*/ return (ushort)( Functions.Low( (ushort) ICHK )) ; 
            
            }
            
        static public ushort NAVCALCULATEOROFBYTESCHECKSUM (  SplusExecutionContext __context__, SplusObject __caller__ , ushort ISTARTBYTE , CrestronString SPARAM ) 
            { 
            ushort X = 0;
            ushort ICHK = 0;
            
            
            /*220*/ ICHK = (ushort) ( 0 ) ; 
            /*221*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( ISTARTBYTE ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( SPARAM ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( X  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (X  >= __FN_FORSTART_VAL__1) && (X  <= __FN_FOREND_VAL__1) ) : ( (X  <= __FN_FORSTART_VAL__1) && (X  >= __FN_FOREND_VAL__1) ) ; X  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*222*/ ICHK = (ushort) ( (ICHK | __caller__.Byte( SPARAM , (int)( X ) )) ) ; 
                /*221*/ } 
            
            /*225*/ return (ushort)( Functions.Low( (ushort) ICHK )) ; 
            
            }
            
        static public short NAVSCALEVALUE (  SplusExecutionContext __context__, SplusObject __caller__ , short SICURRENTVAL , short SIINPUTRANGE , short SIOUTPUTRANGE , short SIOFFSET ) 
            { 
            
            /*229*/ return (short)( (((SICURRENTVAL * SIOUTPUTRANGE) / SIINPUTRANGE) + SIOFFSET)) ; 
            
            }
            
        static public short NAVHALFPOINTOFRANGE (  SplusExecutionContext __context__, SplusObject __caller__ , short SITOP , short SIBOTTOM ) 
            { 
            
            /*233*/ return (short)( (((SITOP - SIBOTTOM) / 2) + SIBOTTOM)) ; 
            
            }
            
        static public short NAVQUARTERPOINTOFRANGE (  SplusExecutionContext __context__, SplusObject __caller__ , short SITOP , short SIBOTTOM ) 
            { 
            
            /*237*/ return (short)( (((SITOP - SIBOTTOM) / 4) + SIBOTTOM)) ; 
            
            }
            
        static public short NAVTHREEQUARTERPOINTOFRANGE (  SplusExecutionContext __context__, SplusObject __caller__ , short SITOP , short SIBOTTOM ) 
            { 
            
            /*241*/ return (short)( ((((SITOP - SIBOTTOM) / 4) * 3) + SIBOTTOM)) ; 
            
            }
            
        static public ushort NAVFINDINARRAYINTEGER (  SplusExecutionContext __context__, SplusObject __caller__ , ushort [] IARRAY , ushort IVAL ) 
            { 
            ushort X = 0;
            
            
            /*247*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.GetNumArrayCols( IARRAY ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( X  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (X  >= __FN_FORSTART_VAL__1) && (X  <= __FN_FOREND_VAL__1) ) : ( (X  <= __FN_FORSTART_VAL__1) && (X  >= __FN_FOREND_VAL__1) ) ; X  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*248*/ if ( Functions.TestForTrue  ( ( Functions.BoolToInt (IARRAY[ X ] == IVAL))  ) ) 
                    { 
                    /*249*/ return (ushort)( X) ; 
                    } 
                
                /*247*/ } 
            
            /*253*/ return (ushort)( 0) ; 
            
            }
            
        static public ushort NAVFINDINARRAYSIGNED_INTEGER (  SplusExecutionContext __context__, SplusObject __caller__ , short [] SIARRAY , short SIVAL ) 
            { 
            ushort X = 0;
            
            
            /*258*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.GetNumArrayCols( SIARRAY ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( X  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (X  >= __FN_FORSTART_VAL__1) && (X  <= __FN_FOREND_VAL__1) ) : ( (X  <= __FN_FORSTART_VAL__1) && (X  >= __FN_FOREND_VAL__1) ) ; X  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*259*/ if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SIARRAY[ X ] == SIVAL))  ) ) 
                    { 
                    /*260*/ return (ushort)( X) ; 
                    } 
                
                /*258*/ } 
            
            /*264*/ return (ushort)( 0) ; 
            
            }
            
        static public ushort NAVFINDINARRAYLONG_INTEGER (  SplusExecutionContext __context__, SplusObject __caller__ , uint [] LIARRAY , uint LIVAL ) 
            { 
            ushort X = 0;
            
            
            /*269*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.GetNumArrayCols( LIARRAY ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( X  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (X  >= __FN_FORSTART_VAL__1) && (X  <= __FN_FOREND_VAL__1) ) : ( (X  <= __FN_FORSTART_VAL__1) && (X  >= __FN_FOREND_VAL__1) ) ; X  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*270*/ if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LIARRAY[ X ] == LIVAL))  ) ) 
                    { 
                    /*271*/ return (ushort)( X) ; 
                    } 
                
                /*269*/ } 
            
            /*275*/ return (ushort)( 0) ; 
            
            }
            
        static public ushort NAVFINDINARRAYSIGNED_LONG_INTEGER (  SplusExecutionContext __context__, SplusObject __caller__ , int [] SLIARRAY , int SLIVAL ) 
            { 
            ushort X = 0;
            
            
            /*280*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.GetNumArrayCols( SLIARRAY ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( X  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (X  >= __FN_FORSTART_VAL__1) && (X  <= __FN_FOREND_VAL__1) ) : ( (X  <= __FN_FORSTART_VAL__1) && (X  >= __FN_FOREND_VAL__1) ) ; X  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*281*/ if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SLIARRAY[ X ] == SLIVAL))  ) ) 
                    { 
                    /*282*/ return (ushort)( X) ; 
                    } 
                
                /*280*/ } 
            
            /*286*/ return (ushort)( 0) ; 
            
            }
            
        static public ushort NAVFINDINARRAYSTRING (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString [] SARRAY , CrestronString SVAL ) 
            { 
            ushort X = 0;
            
            
            /*291*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.GetNumArrayRows( SARRAY ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( X  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (X  >= __FN_FORSTART_VAL__1) && (X  <= __FN_FOREND_VAL__1) ) : ( (X  <= __FN_FORSTART_VAL__1) && (X  >= __FN_FOREND_VAL__1) ) ; X  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*292*/ if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SARRAY[ X ] == SVAL))  ) ) 
                    { 
                    /*293*/ return (ushort)( X) ; 
                    } 
                
                /*291*/ } 
            
            /*297*/ return (ushort)( 0) ; 
            
            }
            
        static public CrestronString NAVFINDANDREPLACE (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SPARAM , CrestronString SFIND , CrestronString SREPLACE ) 
            { 
            ushort X = 0;
            
            CrestronString STEMP;
            STEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1024, __caller__ );
            
            
            /*303*/ STEMP  .UpdateValue ( SPARAM  ) ; 
            /*304*/ while ( Functions.TestForTrue  ( ( NAVCONTAINS( __context__ , __caller__ , STEMP , SFIND ))  ) ) 
                { 
                /*305*/ X = (ushort) ( NAVCONTAINS( __context__ , __caller__ , STEMP , SFIND ) ) ; 
                /*306*/ STEMP  .UpdateValue ( Functions.Left ( SPARAM ,  (int) ( (X - 1) ) ) + SREPLACE + NAVSTRIPCHARSFROMLEFT (  __context__ , __caller__ , SPARAM, (ushort)( ((X - 1) + Functions.Length( SFIND )) ))  ) ; 
                /*304*/ } 
            
            /*309*/ return ( STEMP ) ; 
            
            }
            
        static public CrestronString NAVGETSTRINGBETWEEN (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString CSTRING , CrestronString CSTARTSTRING , CrestronString CENDSTRING ) 
            { 
            ushort [] IPOS;
            IPOS  = new ushort[ 3 ];
            
            
            /*314*/ if ( Functions.TestForTrue  ( ( Functions.Length( CSTRING ))  ) ) 
                { 
                /*315*/ IPOS [ 1] = (ushort) ( Functions.Find( CSTARTSTRING , CSTRING , 1 ) ) ; 
                /*316*/ IPOS [ 2] = (ushort) ( Functions.Find( CENDSTRING , CSTRING , (IPOS[ 1 ] + Functions.Length( CSTARTSTRING )) ) ) ; 
                /*317*/ if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( IPOS[ 1 ] ) && Functions.TestForTrue ( IPOS[ 2 ] )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( IPOS[ 1 ] < IPOS[ 2 ] ) )) ))  ) ) 
                    { 
                    /*318*/ IPOS [ 1] = (ushort) ( (IPOS[ 1 ] + Functions.Length( CSTARTSTRING )) ) ; 
                    /*319*/ return ( Functions.Mid ( CSTRING ,  (int) ( IPOS[ 1 ] ) ,  (int) ( (IPOS[ 2 ] - IPOS[ 1 ]) ) ) ) ; 
                    } 
                
                } 
            
            /*323*/ return ( "" ) ; 
            
            }
            
        static public CrestronString NAVGETSOCKETSTATUSSTRING (  SplusExecutionContext __context__, SplusObject __caller__ , short IPARAM ) 
            { 
            
            /*327*/ 
                {
                int __SPLS_TMPVAR__SWTCH_1__ = ((int)IPARAM);
                
                    { 
                    if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 0) ) ) ) 
                        { 
                        /*328*/ return ( "Socket Status: Not Connected" ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                        { 
                        /*329*/ return ( "Socket Status: Waiting for Connection" ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                        { 
                        /*330*/ return ( "Socket Status: Connected" ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) ) ) ) 
                        { 
                        /*331*/ return ( "Socket Status: Connection Failed" ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 4) ) ) ) 
                        { 
                        /*332*/ return ( "Socket Status: Connection Broken Remotely" ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 5) ) ) ) 
                        { 
                        /*333*/ return ( "Socket Status: Connection Broken Locally" ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 6) ) ) ) 
                        { 
                        /*334*/ return ( "Socket Status: Performing DNS Lookup" ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 7) ) ) ) 
                        { 
                        /*335*/ return ( "Socket Status: DNS Lookup Failed" ) ; 
                        } 
                    
                    else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 8) ) ) ) 
                        { 
                        /*336*/ return ( "Socket Status: DNS Name Resolved" ) ; 
                        } 
                    
                    } 
                    
                }
                
            
            
            return ""; // default return value (none specified in module)
            }
            
        static public CrestronString BINL2HEX (  SplusExecutionContext __context__, SplusObject __caller__ , ref int [] BINARRAY ) 
            { 
            ushort I = 0;
            
            CrestronString STR;
            STR  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 128, __caller__ );
            
            ushort [] HEX_TAB;
            HEX_TAB  = new ushort[ 17 ];
            
            
            /*345*/ HEX_TAB [ 0] = (ushort) ( 48 ) ; 
            /*346*/ HEX_TAB [ 1] = (ushort) ( 49 ) ; 
            /*347*/ HEX_TAB [ 2] = (ushort) ( 50 ) ; 
            /*348*/ HEX_TAB [ 3] = (ushort) ( 51 ) ; 
            /*349*/ HEX_TAB [ 4] = (ushort) ( 52 ) ; 
            /*350*/ HEX_TAB [ 5] = (ushort) ( 53 ) ; 
            /*351*/ HEX_TAB [ 6] = (ushort) ( 54 ) ; 
            /*352*/ HEX_TAB [ 7] = (ushort) ( 55 ) ; 
            /*353*/ HEX_TAB [ 8] = (ushort) ( 56 ) ; 
            /*354*/ HEX_TAB [ 9] = (ushort) ( 57 ) ; 
            /*355*/ HEX_TAB [ 10] = (ushort) ( 97 ) ; 
            /*356*/ HEX_TAB [ 11] = (ushort) ( 98 ) ; 
            /*357*/ HEX_TAB [ 12] = (ushort) ( 99 ) ; 
            /*358*/ HEX_TAB [ 13] = (ushort) ( 100 ) ; 
            /*359*/ HEX_TAB [ 14] = (ushort) ( 101 ) ; 
            /*360*/ HEX_TAB [ 15] = (ushort) ( 102 ) ; 
            /*362*/ I = (ushort) ( 0 ) ; 
            /*363*/ while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( I < 16 ))  ) ) 
                { 
                /*365*/ STR  .UpdateValue ( STR + Functions.Chr (  (int) ( HEX_TAB[ ((BINARRAY[ (I >> 2) ] >> ((__caller__.Mod( I , 4 ) * 8) + 4)) & 15) ] ) ) + Functions.Chr (  (int) ( HEX_TAB[ ((BINARRAY[ (I >> 2) ] >> (__caller__.Mod( I , 4 ) * 8)) & 15) ] ) )  ) ; 
                /*366*/ I = (ushort) ( (I + 1) ) ; 
                /*363*/ } 
            
            /*369*/ return ( STR ) ; 
            
            }
            
        static public int SAFE_ADD (  SplusExecutionContext __context__, SplusObject __caller__ , int X , int Y ) 
            { 
            int LSW = 0;
            int MSW = 0;
            
            
            /*375*/ LSW = (int) ( ((X & 65535) + (Y & 65535)) ) ; 
            /*376*/ MSW = (int) ( (((X >> 16) + (Y >> 16)) + (LSW >> 16)) ) ; 
            /*378*/ return (int)( ((MSW << 16) | (LSW & 65535))) ; 
            
            }
            
        static public int BIT_ROL (  SplusExecutionContext __context__, SplusObject __caller__ , int NUM , short CNT ) 
            { 
            
            /*383*/ return (int)( (Functions.RotateLeftLong( (uint)( NUM ) , (uint)( CNT ) ) | Functions.RotateRightLong( (uint)( NUM ) , (uint)( (32 - CNT) ) ))) ; 
            
            }
            
        static public int MD5_CMN (  SplusExecutionContext __context__, SplusObject __caller__ , int Q , int A , int B , int X , short S , int T ) 
            { 
            
            /*388*/ return (int)( SAFE_ADD( __context__ , __caller__ , (int)( BIT_ROL( __context__ , __caller__ , (int)( SAFE_ADD( __context__ , __caller__ , (int)( SAFE_ADD( __context__ , __caller__ , (int)( A ) , (int)( Q ) ) ) , (int)( SAFE_ADD( __context__ , __caller__ , (int)( X ) , (int)( T ) ) ) ) ) , (short)( S ) ) ) , (int)( B ) )) ; 
            
            }
            
        static public int MD5_FF (  SplusExecutionContext __context__, SplusObject __caller__ , int A , int B , int C , int D , int X , short S , int T ) 
            { 
            
            /*393*/ return (int)( MD5_CMN( __context__ , __caller__ , (int)( ((B & C) | (Functions.OnesComplement( B ) & D)) ) , (int)( A ) , (int)( B ) , (int)( X ) , (short)( S ) , (int)( T ) )) ; 
            
            }
            
        static public int MD5_GG (  SplusExecutionContext __context__, SplusObject __caller__ , int A , int B , int C , int D , int X , short S , int T ) 
            { 
            
            /*398*/ return (int)( MD5_CMN( __context__ , __caller__ , (int)( ((B & D) | (C & Functions.OnesComplement( D ))) ) , (int)( A ) , (int)( B ) , (int)( X ) , (short)( S ) , (int)( T ) )) ; 
            
            }
            
        static public int MD5_HH (  SplusExecutionContext __context__, SplusObject __caller__ , int A , int B , int C , int D , int X , short S , int T ) 
            { 
            
            /*403*/ return (int)( MD5_CMN( __context__ , __caller__ , (int)( ((B ^ C) ^ D) ) , (int)( A ) , (int)( B ) , (int)( X ) , (short)( S ) , (int)( T ) )) ; 
            
            }
            
        static public int MD5_II (  SplusExecutionContext __context__, SplusObject __caller__ , int A , int B , int C , int D , int X , short S , int T ) 
            { 
            
            /*408*/ return (int)( MD5_CMN( __context__ , __caller__ , (int)( (C ^ (B | Functions.OnesComplement( D ))) ) , (int)( A ) , (int)( B ) , (int)( X ) , (short)( S ) , (int)( T ) )) ; 
            
            }
            
        static public CrestronString CORE_MD5 (  SplusExecutionContext __context__, SplusObject __caller__ , ushort LENGTH , ref int [] BIN ) 
            { 
            ushort I = 0;
            
            int A = 0;
            int B = 0;
            int C = 0;
            int D = 0;
            int OLDA = 0;
            int OLDB = 0;
            int OLDC = 0;
            int OLDD = 0;
            int [] BINARRAY;
            BINARRAY  = new int[ 5 ];
            
            
            /*415*/ BIN [ (LENGTH >> 5)] = (int) ( (BIN[ (LENGTH >> 5) ] | (128 << __caller__.Mod( LENGTH , 32 ))) ) ; 
            /*416*/ BIN [ (Crestron.Logos.SplusLibrary.Functions.RotateLeftLong( (uint)((LENGTH + 64) >> 9) , 4 ) + 14)] = (int) ( LENGTH ) ; 
            /*418*/ A = (int) ( 1732584193 ) ; 
            /*419*/ B = (int) ( Functions.ToSignedLongInteger( -( 271733879 ) ) ) ; 
            /*420*/ C = (int) ( Functions.ToSignedLongInteger( -( 1732584194 ) ) ) ; 
            /*421*/ D = (int) ( 271733878 ) ; 
            /*423*/ I = (ushort) ( 0 ) ; 
            /*424*/ while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( I < 15 ))  ) ) 
                { 
                /*426*/ OLDA = (int) ( A ) ; 
                /*427*/ OLDB = (int) ( B ) ; 
                /*428*/ OLDC = (int) ( C ) ; 
                /*429*/ OLDD = (int) ( D ) ; 
                /*431*/ A = (int) ( MD5_FF( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 0) ] ) , (short)( 7 ) , (int)( Functions.ToSignedLongInteger( -( 680876936 ) ) ) ) ) ; 
                /*432*/ D = (int) ( MD5_FF( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 1) ] ) , (short)( 12 ) , (int)( Functions.ToSignedLongInteger( -( 389564586 ) ) ) ) ) ; 
                /*433*/ C = (int) ( MD5_FF( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 2) ] ) , (short)( 17 ) , (int)( 606105819 ) ) ) ; 
                /*434*/ B = (int) ( MD5_FF( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 3) ] ) , (short)( 22 ) , (int)( Functions.ToSignedLongInteger( -( 1044525330 ) ) ) ) ) ; 
                /*435*/ A = (int) ( MD5_FF( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 4) ] ) , (short)( 7 ) , (int)( Functions.ToSignedLongInteger( -( 176418897 ) ) ) ) ) ; 
                /*436*/ D = (int) ( MD5_FF( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 5) ] ) , (short)( 12 ) , (int)( 1200080426 ) ) ) ; 
                /*437*/ C = (int) ( MD5_FF( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 6) ] ) , (short)( 17 ) , (int)( Functions.ToSignedLongInteger( -( 1473231341 ) ) ) ) ) ; 
                /*438*/ B = (int) ( MD5_FF( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 7) ] ) , (short)( 22 ) , (int)( Functions.ToSignedLongInteger( -( 45705983 ) ) ) ) ) ; 
                /*439*/ A = (int) ( MD5_FF( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 8) ] ) , (short)( 7 ) , (int)( 1770035416 ) ) ) ; 
                /*440*/ D = (int) ( MD5_FF( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 9) ] ) , (short)( 12 ) , (int)( Functions.ToSignedLongInteger( -( 1958414417 ) ) ) ) ) ; 
                /*441*/ C = (int) ( MD5_FF( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 10) ] ) , (short)( 17 ) , (int)( Functions.ToSignedLongInteger( -( 42063 ) ) ) ) ) ; 
                /*442*/ B = (int) ( MD5_FF( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 11) ] ) , (short)( 22 ) , (int)( Functions.ToSignedLongInteger( -( 1990404162 ) ) ) ) ) ; 
                /*443*/ A = (int) ( MD5_FF( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 12) ] ) , (short)( 7 ) , (int)( 1804603682 ) ) ) ; 
                /*444*/ D = (int) ( MD5_FF( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 13) ] ) , (short)( 12 ) , (int)( Functions.ToSignedLongInteger( -( 40341101 ) ) ) ) ) ; 
                /*445*/ C = (int) ( MD5_FF( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 14) ] ) , (short)( 17 ) , (int)( Functions.ToSignedLongInteger( -( 1502002290 ) ) ) ) ) ; 
                /*446*/ B = (int) ( MD5_FF( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 15) ] ) , (short)( 22 ) , (int)( 1236535329 ) ) ) ; 
                /*448*/ A = (int) ( MD5_GG( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 1) ] ) , (short)( 5 ) , (int)( Functions.ToSignedLongInteger( -( 165796510 ) ) ) ) ) ; 
                /*449*/ D = (int) ( MD5_GG( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 6) ] ) , (short)( 9 ) , (int)( Functions.ToSignedLongInteger( -( 1069501632 ) ) ) ) ) ; 
                /*450*/ C = (int) ( MD5_GG( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 11) ] ) , (short)( 14 ) , (int)( 643717713 ) ) ) ; 
                /*451*/ B = (int) ( MD5_GG( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 0) ] ) , (short)( 20 ) , (int)( Functions.ToSignedLongInteger( -( 373897302 ) ) ) ) ) ; 
                /*452*/ A = (int) ( MD5_GG( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 5) ] ) , (short)( 5 ) , (int)( Functions.ToSignedLongInteger( -( 701558691 ) ) ) ) ) ; 
                /*453*/ D = (int) ( MD5_GG( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 10) ] ) , (short)( 9 ) , (int)( 38016083 ) ) ) ; 
                /*454*/ C = (int) ( MD5_GG( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 15) ] ) , (short)( 14 ) , (int)( Functions.ToSignedLongInteger( -( 660478335 ) ) ) ) ) ; 
                /*455*/ B = (int) ( MD5_GG( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 4) ] ) , (short)( 20 ) , (int)( Functions.ToSignedLongInteger( -( 405537848 ) ) ) ) ) ; 
                /*456*/ A = (int) ( MD5_GG( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 9) ] ) , (short)( 5 ) , (int)( 568446438 ) ) ) ; 
                /*457*/ D = (int) ( MD5_GG( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 14) ] ) , (short)( 9 ) , (int)( Functions.ToSignedLongInteger( -( 1019803690 ) ) ) ) ) ; 
                /*458*/ C = (int) ( MD5_GG( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 3) ] ) , (short)( 14 ) , (int)( Functions.ToSignedLongInteger( -( 187363961 ) ) ) ) ) ; 
                /*459*/ B = (int) ( MD5_GG( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 8) ] ) , (short)( 20 ) , (int)( 1163531501 ) ) ) ; 
                /*460*/ A = (int) ( MD5_GG( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 13) ] ) , (short)( 5 ) , (int)( Functions.ToSignedLongInteger( -( 1444681467 ) ) ) ) ) ; 
                /*461*/ D = (int) ( MD5_GG( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 2) ] ) , (short)( 9 ) , (int)( Functions.ToSignedLongInteger( -( 51403784 ) ) ) ) ) ; 
                /*462*/ C = (int) ( MD5_GG( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 7) ] ) , (short)( 14 ) , (int)( 1735328473 ) ) ) ; 
                /*463*/ B = (int) ( MD5_GG( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 12) ] ) , (short)( 20 ) , (int)( Functions.ToSignedLongInteger( -( 1926607734 ) ) ) ) ) ; 
                /*465*/ A = (int) ( MD5_HH( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 5) ] ) , (short)( 4 ) , (int)( Functions.ToSignedLongInteger( -( 378558 ) ) ) ) ) ; 
                /*466*/ D = (int) ( MD5_HH( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 8) ] ) , (short)( 11 ) , (int)( Functions.ToSignedLongInteger( -( 2022574463 ) ) ) ) ) ; 
                /*467*/ C = (int) ( MD5_HH( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 11) ] ) , (short)( 16 ) , (int)( 1839030562 ) ) ) ; 
                /*468*/ B = (int) ( MD5_HH( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 14) ] ) , (short)( 23 ) , (int)( Functions.ToSignedLongInteger( -( 35309556 ) ) ) ) ) ; 
                /*469*/ A = (int) ( MD5_HH( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 1) ] ) , (short)( 4 ) , (int)( Functions.ToSignedLongInteger( -( 1530992060 ) ) ) ) ) ; 
                /*470*/ D = (int) ( MD5_HH( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 4) ] ) , (short)( 11 ) , (int)( 1272893353 ) ) ) ; 
                /*471*/ C = (int) ( MD5_HH( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 7) ] ) , (short)( 16 ) , (int)( Functions.ToSignedLongInteger( -( 155497632 ) ) ) ) ) ; 
                /*472*/ B = (int) ( MD5_HH( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 10) ] ) , (short)( 23 ) , (int)( Functions.ToSignedLongInteger( -( 1094730640 ) ) ) ) ) ; 
                /*473*/ A = (int) ( MD5_HH( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 13) ] ) , (short)( 4 ) , (int)( 681279174 ) ) ) ; 
                /*474*/ D = (int) ( MD5_HH( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 0) ] ) , (short)( 11 ) , (int)( Functions.ToSignedLongInteger( -( 358537222 ) ) ) ) ) ; 
                /*475*/ C = (int) ( MD5_HH( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 3) ] ) , (short)( 16 ) , (int)( Functions.ToSignedLongInteger( -( 722521979 ) ) ) ) ) ; 
                /*476*/ B = (int) ( MD5_HH( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 6) ] ) , (short)( 23 ) , (int)( 76029189 ) ) ) ; 
                /*477*/ A = (int) ( MD5_HH( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 9) ] ) , (short)( 4 ) , (int)( Functions.ToSignedLongInteger( -( 640364487 ) ) ) ) ) ; 
                /*478*/ D = (int) ( MD5_HH( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 12) ] ) , (short)( 11 ) , (int)( Functions.ToSignedLongInteger( -( 421815835 ) ) ) ) ) ; 
                /*479*/ C = (int) ( MD5_HH( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 15) ] ) , (short)( 16 ) , (int)( 530742520 ) ) ) ; 
                /*480*/ B = (int) ( MD5_HH( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 2) ] ) , (short)( 23 ) , (int)( Functions.ToSignedLongInteger( -( 995338651 ) ) ) ) ) ; 
                /*482*/ A = (int) ( MD5_II( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 0) ] ) , (short)( 6 ) , (int)( Functions.ToSignedLongInteger( -( 198630844 ) ) ) ) ) ; 
                /*483*/ D = (int) ( MD5_II( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 7) ] ) , (short)( 10 ) , (int)( 1126891415 ) ) ) ; 
                /*484*/ C = (int) ( MD5_II( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 14) ] ) , (short)( 15 ) , (int)( Functions.ToSignedLongInteger( -( 1416354905 ) ) ) ) ) ; 
                /*485*/ B = (int) ( MD5_II( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 5) ] ) , (short)( 21 ) , (int)( Functions.ToSignedLongInteger( -( 57434055 ) ) ) ) ) ; 
                /*486*/ A = (int) ( MD5_II( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 12) ] ) , (short)( 6 ) , (int)( 1700485571 ) ) ) ; 
                /*487*/ D = (int) ( MD5_II( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 3) ] ) , (short)( 10 ) , (int)( Functions.ToSignedLongInteger( -( 1894986606 ) ) ) ) ) ; 
                /*488*/ C = (int) ( MD5_II( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 10) ] ) , (short)( 15 ) , (int)( Functions.ToSignedLongInteger( -( 1051523 ) ) ) ) ) ; 
                /*489*/ B = (int) ( MD5_II( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 1) ] ) , (short)( 21 ) , (int)( Functions.ToSignedLongInteger( -( 2054922799 ) ) ) ) ) ; 
                /*490*/ A = (int) ( MD5_II( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 8) ] ) , (short)( 6 ) , (int)( 1873313359 ) ) ) ; 
                /*491*/ D = (int) ( MD5_II( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 15) ] ) , (short)( 10 ) , (int)( Functions.ToSignedLongInteger( -( 30611744 ) ) ) ) ) ; 
                /*492*/ C = (int) ( MD5_II( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 06) ] ) , (short)( 15 ) , (int)( Functions.ToSignedLongInteger( -( 1560198380 ) ) ) ) ) ; 
                /*493*/ B = (int) ( MD5_II( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 13) ] ) , (short)( 21 ) , (int)( 1309151649 ) ) ) ; 
                /*494*/ A = (int) ( MD5_II( __context__ , __caller__ , (int)( A ) , (int)( B ) , (int)( C ) , (int)( D ) , (int)( BIN[ (I + 4) ] ) , (short)( 6 ) , (int)( Functions.ToSignedLongInteger( -( 145523070 ) ) ) ) ) ; 
                /*495*/ D = (int) ( MD5_II( __context__ , __caller__ , (int)( D ) , (int)( A ) , (int)( B ) , (int)( C ) , (int)( BIN[ (I + 11) ] ) , (short)( 10 ) , (int)( Functions.ToSignedLongInteger( -( 1120210379 ) ) ) ) ) ; 
                /*496*/ C = (int) ( MD5_II( __context__ , __caller__ , (int)( C ) , (int)( D ) , (int)( A ) , (int)( B ) , (int)( BIN[ (I + 2) ] ) , (short)( 15 ) , (int)( 718787259 ) ) ) ; 
                /*497*/ B = (int) ( MD5_II( __context__ , __caller__ , (int)( B ) , (int)( C ) , (int)( D ) , (int)( A ) , (int)( BIN[ (I + 9) ] ) , (short)( 21 ) , (int)( Functions.ToSignedLongInteger( -( 343485551 ) ) ) ) ) ; 
                /*499*/ A = (int) ( SAFE_ADD( __context__ , __caller__ , (int)( A ) , (int)( OLDA ) ) ) ; 
                /*500*/ B = (int) ( SAFE_ADD( __context__ , __caller__ , (int)( B ) , (int)( OLDB ) ) ) ; 
                /*501*/ C = (int) ( SAFE_ADD( __context__ , __caller__ , (int)( C ) , (int)( OLDC ) ) ) ; 
                /*502*/ D = (int) ( SAFE_ADD( __context__ , __caller__ , (int)( D ) , (int)( OLDD ) ) ) ; 
                /*504*/ I = (ushort) ( (I + 16) ) ; 
                /*424*/ } 
            
            /*506*/ BINARRAY [ 0] = (int) ( A ) ; 
            /*507*/ BINARRAY [ 1] = (int) ( B ) ; 
            /*508*/ BINARRAY [ 2] = (int) ( C ) ; 
            /*509*/ BINARRAY [ 3] = (int) ( D ) ; 
            /*510*/ return ( BINL2HEX (  __context__ , __caller__ ,   ref  BINARRAY ) ) ; 
            
            }
            
        static public void STR2BINL (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString STR , ref int [] BIN ) 
            { 
            ushort I = 0;
            ushort MASK = 0;
            
            int ONE = 0;
            int TEMP__DOLLAR__ = 0;
            
            
            /*518*/ ONE = (int) ( 1 ) ; 
            /*519*/ MASK = (ushort) ( ((ONE << 8) - 1) ) ; 
            /*520*/ I = (ushort) ( 0 ) ; 
            /*521*/ while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( I < (Functions.Length( STR ) * 8) ))  ) ) 
                { 
                /*523*/ BIN [ (I >> 5)] = (int) ( (BIN[ (I >> 5) ] | ((__caller__.Byte( STR , (int)( ((I / 8) + 1) ) ) & MASK) << __caller__.Mod( I , 32 ))) ) ; 
                /*524*/ I = (ushort) ( (I + 8) ) ; 
                /*521*/ } 
            
            
            }
            
        static public CrestronString NAVHEXMD5 (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString S ) 
            { 
            int [] BIN;
            BIN  = new int[ 2001 ];
            
            
            /*531*/ Functions.SetArray (  ref BIN , (int)0) ; 
            /*532*/ STR2BINL (  __context__ , __caller__ , S,   ref  BIN ) ; 
            /*533*/ return ( CORE_MD5 (  __context__ , __caller__ , (ushort)( (Functions.Length( S ) * Functions.LowWord( (uint)( 8 ) )) ),   ref  BIN ) ) ; 
            
            }
            
        static public void NAVCONSTANTS (  SplusExecutionContext __context__, SplusObject __caller__ , ref ushort [] ENCS , ref ushort [] DECS , uint [,] ENCMIX , uint [,] DECMIX ) 
            { 
            ushort I = 0;
            ushort X = 0;
            ushort X2 = 0;
            ushort X4 = 0;
            ushort X8 = 0;
            ushort XINV = 0;
            ushort S = 0;
            ushort [] D;
            ushort [] T;
            D  = new ushort[ 256 ];
            T  = new ushort[ 256 ];
            
            uint TENC = 0;
            uint TDEC = 0;
            
            
            /*545*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)255; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*546*/ D [ I] = (ushort) ( ((I << 1) ^ ((I >> 7) * 283)) ) ; 
                /*547*/ T [ (D[ I ] ^ I)] = (ushort) ( I ) ; 
                /*545*/ } 
            
            /*550*/ X = (ushort) ( 0 ) ; 
            /*551*/ XINV = (ushort) ( 0 ) ; 
            /*552*/ while ( Functions.TestForTrue  ( ( Functions.Not( ENCS[ X ] ))  ) ) 
                { 
                /*554*/ S = (ushort) ( ((((XINV ^ (XINV << 1)) ^ (XINV << 2)) ^ (XINV << 3)) ^ (XINV << 4)) ) ; 
                /*555*/ S = (ushort) ( (((S >> 8) ^ (S & 255)) ^ 99) ) ; 
                /*556*/ ENCS [ X] = (ushort) ( S ) ; 
                /*557*/ DECS [ S] = (ushort) ( X ) ; 
                /*560*/ X2 = (ushort) ( D[ X ] ) ; 
                /*561*/ X4 = (ushort) ( D[ X2 ] ) ; 
                /*562*/ X8 = (ushort) ( D[ X4 ] ) ; 
                /*563*/ TDEC = (uint) ( ((((X8 * 16843009) ^ (X4 * 65537)) ^ (X2 * 257)) ^ (X * 16843008)) ) ; 
                /*564*/ TENC = (uint) ( ((D[ S ] * 257) ^ (S * 16843008)) ) ; 
                /*566*/ ushort __FN_FORSTART_VAL__2 = (ushort) ( 0 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)3; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( I  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (I  >= __FN_FORSTART_VAL__2) && (I  <= __FN_FOREND_VAL__2) ) : ( (I  <= __FN_FORSTART_VAL__2) && (I  >= __FN_FOREND_VAL__2) ) ; I  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    /*567*/ TENC = (uint) ( ((TENC << 24) ^ (TENC >> 8)) ) ; 
                    /*568*/ ENCMIX [ I , X] = (uint) ( TENC ) ; 
                    /*569*/ TDEC = (uint) ( ((TDEC << 24) ^ (TDEC >> 8)) ) ; 
                    /*570*/ DECMIX [ I , S] = (uint) ( TDEC ) ; 
                    /*566*/ } 
                
                /*573*/ if ( Functions.TestForTrue  ( ( Functions.BoolToInt (X2 == 0))  ) ) 
                    {
                    /*574*/ X = (ushort) ( (X ^ 1) ) ; 
                    }
                
                else 
                    {
                    /*576*/ X = (ushort) ( (X ^ X2) ) ; 
                    }
                
                /*578*/ XINV = (ushort) ( T[ XINV ] ) ; 
                /*579*/ if ( Functions.TestForTrue  ( ( Functions.BoolToInt (XINV == 0))  ) ) 
                    {
                    /*580*/ XINV = (ushort) ( 1 ) ; 
                    }
                
                /*552*/ } 
            
            
            }
            
        static public void NAVKEYSCHEDULE (  SplusExecutionContext __context__, SplusObject __caller__ , ref ushort [] ENCS , uint [,] DECMIX , ref uint [] ENCKEY , ref uint [] DECKEY , ref ushort KEYGOOD ) 
            { 
            ushort I = 0;
            ushort J = 0;
            
            uint TMP = 0;
            uint RCON = 0;
            
            
            /*587*/ RCON = (uint) ( 1 ) ; 
            /*589*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 4 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)43; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*590*/ TMP = (uint) ( ENCKEY[ (I - 1) ] ) ; 
                /*591*/ if ( Functions.TestForTrue  ( ( Functions.Not( (I & 3) ))  ) ) 
                    { 
                    /*592*/ TMP = (uint) ( ((((ENCS[ (TMP >> 24) ] << 24) ^ (ENCS[ ((TMP >> 16) & 255) ] << 16)) ^ (ENCS[ ((TMP >> 8) & 255) ] << 8)) ^ ENCS[ (TMP & 255) ]) ) ; 
                    /*593*/ TMP = (uint) ( (((TMP << 8) ^ (TMP >> 24)) ^ (RCON << 24)) ) ; 
                    /*594*/ RCON = (uint) ( ((RCON << 1) ^ ((RCON >> 7) * 283)) ) ; 
                    } 
                
                /*597*/ ENCKEY [ I] = (uint) ( (ENCKEY[ (I - 4) ] ^ TMP) ) ; 
                /*589*/ } 
            
            /*601*/ DECKEY [ 0] = (uint) ( ENCKEY[ 40 ] ) ; 
            /*602*/ DECKEY [ 1] = (uint) ( ENCKEY[ 43 ] ) ; 
            /*603*/ DECKEY [ 2] = (uint) ( ENCKEY[ 42 ] ) ; 
            /*604*/ DECKEY [ 3] = (uint) ( ENCKEY[ 41 ] ) ; 
            /*605*/ DECKEY [ 40] = (uint) ( ENCKEY[ 0 ] ) ; 
            /*606*/ DECKEY [ 41] = (uint) ( ENCKEY[ 3 ] ) ; 
            /*607*/ DECKEY [ 42] = (uint) ( ENCKEY[ 2 ] ) ; 
            /*608*/ DECKEY [ 43] = (uint) ( ENCKEY[ 1 ] ) ; 
            /*609*/ ushort __FN_FORSTART_VAL__2 = (ushort) ( 4 ) ;
            ushort __FN_FOREND_VAL__2 = (ushort)39; 
            int __FN_FORSTEP_VAL__2 = (int)1; 
            for ( J  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (J  >= __FN_FORSTART_VAL__2) && (J  <= __FN_FOREND_VAL__2) ) : ( (J  <= __FN_FORSTART_VAL__2) && (J  >= __FN_FOREND_VAL__2) ) ; J  += (ushort)__FN_FORSTEP_VAL__2) 
                { 
                /*610*/ I = (ushort) ( (44 - J) ) ; 
                /*611*/ if ( Functions.TestForTrue  ( ( (J & 3))  ) ) 
                    {
                    /*612*/ TMP = (uint) ( ENCKEY[ I ] ) ; 
                    }
                
                else 
                    {
                    /*614*/ TMP = (uint) ( ENCKEY[ (I - 4) ] ) ; 
                    }
                
                /*616*/ DECKEY [ J] = (uint) ( (((DECMIX[ 0 , ENCS[ (TMP >> 24) ] ] ^ DECMIX[ 1 , ENCS[ ((TMP >> 16) & 255) ] ]) ^ DECMIX[ 2 , ENCS[ ((TMP >> 8) & 255) ] ]) ^ DECMIX[ 3 , ENCS[ (TMP & 255) ] ]) ) ; 
                /*609*/ } 
            
            /*619*/ KEYGOOD = (ushort) ( 1 ) ; 
            
            }
            
        static public void NAVSETKEY (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SKEY , ref uint [] ENCKEY , ref ushort KEYGOOD ) 
            { 
            CrestronString TMP1;
            TMP1  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16, __caller__ );
            
            ushort I = 0;
            ushort L = 0;
            
            
            /*625*/ KEYGOOD = (ushort) ( 0 ) ; 
            /*627*/ TMP1  .UpdateValue ( SKEY  ) ; 
            /*628*/ L = (ushort) ( Functions.Length( TMP1 ) ) ; 
            /*629*/ while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( L < 16 ))  ) ) 
                { 
                /*630*/ __caller__.SetString ( "\u0000" , (int)(L + 1), TMP1 ) ; 
                /*631*/ L = (ushort) ( Functions.Length( TMP1 ) ) ; 
                /*629*/ } 
            
            /*634*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)3; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*635*/ ENCKEY [ I] = (uint) ( ((((__caller__.Byte( TMP1 , (int)( ((I * 4) + 1) ) ) << 24) | (__caller__.Byte( TMP1 , (int)( ((I * 4) + 2) ) ) << 16)) | (__caller__.Byte( TMP1 , (int)( ((I * 4) + 3) ) ) << 8)) | __caller__.Byte( TMP1 , (int)( ((I * 4) + 4) ) )) ) ; 
                /*634*/ } 
            
            
            }
            
        static public void NAVCIPHER (  SplusExecutionContext __context__, SplusObject __caller__ , ref uint [] KEY , uint [,] MIX , ref ushort [] SBOX , ref uint [] BLOCK ) 
            { 
            uint A = 0;
            uint B = 0;
            uint C = 0;
            uint D = 0;
            uint E = 0;
            uint F = 0;
            uint G = 0;
            
            ushort I = 0;
            ushort KEYOFFSET = 0;
            
            
            /*646*/ A = (uint) ( (BLOCK[ 0 ] ^ KEY[ 0 ]) ) ; 
            /*647*/ B = (uint) ( (BLOCK[ 1 ] ^ KEY[ 1 ]) ) ; 
            /*648*/ C = (uint) ( (BLOCK[ 2 ] ^ KEY[ 2 ]) ) ; 
            /*649*/ D = (uint) ( (BLOCK[ 3 ] ^ KEY[ 3 ]) ) ; 
            /*650*/ KEYOFFSET = (ushort) ( 4 ) ; 
            /*653*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)9; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*654*/ E = (uint) ( ((((MIX[ 0 , (A >> 24) ] ^ MIX[ 1 , ((B >> 16) & 255) ]) ^ MIX[ 2 , ((C >> 8) & 255) ]) ^ MIX[ 3 , (D & 255) ]) ^ KEY[ KEYOFFSET ]) ) ; 
                /*655*/ F = (uint) ( ((((MIX[ 0 , (B >> 24) ] ^ MIX[ 1 , ((C >> 16) & 255) ]) ^ MIX[ 2 , ((D >> 8) & 255) ]) ^ MIX[ 3 , (A & 255) ]) ^ KEY[ (KEYOFFSET + 1) ]) ) ; 
                /*656*/ G = (uint) ( ((((MIX[ 0 , (C >> 24) ] ^ MIX[ 1 , ((D >> 16) & 255) ]) ^ MIX[ 2 , ((A >> 8) & 255) ]) ^ MIX[ 3 , (B & 255) ]) ^ KEY[ (KEYOFFSET + 2) ]) ) ; 
                /*657*/ D = (uint) ( ((((MIX[ 0 , (D >> 24) ] ^ MIX[ 1 , ((A >> 16) & 255) ]) ^ MIX[ 2 , ((B >> 8) & 255) ]) ^ MIX[ 3 , (C & 255) ]) ^ KEY[ (KEYOFFSET + 3) ]) ) ; 
                /*658*/ KEYOFFSET = (ushort) ( (KEYOFFSET + 4) ) ; 
                /*659*/ A = (uint) ( E ) ; 
                /*659*/ B = (uint) ( F ) ; 
                /*659*/ C = (uint) ( G ) ; 
                /*653*/ } 
            
            /*663*/ ushort __FN_FORSTART_VAL__2 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__2 = (ushort)3; 
            int __FN_FORSTEP_VAL__2 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (I  >= __FN_FORSTART_VAL__2) && (I  <= __FN_FOREND_VAL__2) ) : ( (I  <= __FN_FORSTART_VAL__2) && (I  >= __FN_FOREND_VAL__2) ) ; I  += (ushort)__FN_FORSTEP_VAL__2) 
                { 
                /*664*/ BLOCK [ I] = (uint) ( (((((SBOX[ (A >> 24) ] << 24) ^ (SBOX[ ((B >> 16) & 255) ] << 16)) ^ (SBOX[ ((C >> 8) & 255) ] << 8)) ^ SBOX[ (D & 255) ]) ^ KEY[ KEYOFFSET ]) ) ; 
                /*665*/ KEYOFFSET = (ushort) ( (KEYOFFSET + 1) ) ; 
                /*666*/ E = (uint) ( A ) ; 
                /*666*/ A = (uint) ( B ) ; 
                /*666*/ B = (uint) ( C ) ; 
                /*666*/ C = (uint) ( D ) ; 
                /*666*/ D = (uint) ( E ) ; 
                /*663*/ } 
            
            
            }
            
        static public CrestronString NAVENCRYPT (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString S , ref uint [] ENCKEY , uint [,] ENCMIX , ref ushort [] ENCS ) 
            { 
            ushort I = 0;
            ushort J = 0;
            
            CrestronString A;
            CrestronString C;
            A  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4, __caller__ );
            C  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16, __caller__ );
            
            uint [] BLOCK;
            BLOCK  = new uint[ 4 ];
            
            
            /*674*/ A  .UpdateValue ( "1234"  ) ; 
            /*676*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)3; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*677*/ J = (ushort) ( ((I * 4) + 1) ) ; 
                /*678*/ BLOCK [ I] = (uint) ( ((((__caller__.Byte( S , (int)( J ) ) << 24) | (__caller__.Byte( S , (int)( (J + 1) ) ) << 16)) | (__caller__.Byte( S , (int)( (J + 2) ) ) << 8)) | __caller__.Byte( S , (int)( (J + 3) ) )) ) ; 
                /*676*/ } 
            
            /*681*/ NAVCIPHER (  __context__ , __caller__ ,   ref  ENCKEY , (uint[,])( ENCMIX ),   ref  ENCS ,   ref  BLOCK ) ; 
            /*683*/ ushort __FN_FORSTART_VAL__2 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__2 = (ushort)3; 
            int __FN_FORSTEP_VAL__2 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (I  >= __FN_FORSTART_VAL__2) && (I  <= __FN_FOREND_VAL__2) ) : ( (I  <= __FN_FORSTART_VAL__2) && (I  >= __FN_FOREND_VAL__2) ) ; I  += (ushort)__FN_FORSTEP_VAL__2) 
                { 
                /*684*/ __caller__.SetByte ( A ,  (ushort) ( 1 ) ,  (ushort) ( Functions.Low( (ushort) (BLOCK[ I ] >> 24) ) ) ) ; 
                /*685*/ __caller__.SetByte ( A ,  (ushort) ( 2 ) ,  (ushort) ( Functions.Low( (ushort) (BLOCK[ I ] >> 16) ) ) ) ; 
                /*686*/ __caller__.SetByte ( A ,  (ushort) ( 3 ) ,  (ushort) ( Functions.Low( (ushort) (BLOCK[ I ] >> 8) ) ) ) ; 
                /*687*/ __caller__.SetByte ( A ,  (ushort) ( 4 ) ,  (ushort) ( Functions.Low( (ushort) BLOCK[ I ] ) ) ) ; 
                /*689*/ __caller__.SetString ( A , (int)((I * 4) + 1), C ) ; 
                /*683*/ } 
            
            /*692*/ return ( C ) ; 
            
            }
            
        static public CrestronString NAVDECRYPT (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString S , ref uint [] DECKEY , uint [,] DECMIX , ref ushort [] DECS ) 
            { 
            ushort I = 0;
            ushort J = 0;
            
            CrestronString A;
            CrestronString P;
            A  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4, __caller__ );
            P  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16, __caller__ );
            
            uint [] BLOCK;
            BLOCK  = new uint[ 4 ];
            
            
            /*699*/ A  .UpdateValue ( "1234"  ) ; 
            /*700*/ ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)3; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                /*701*/ J = (ushort) ( ((I * 4) + 1) ) ; 
                /*702*/ BLOCK [ (3 & Functions.ToLongInteger( -( I ) ))] = (uint) ( ((((__caller__.Byte( S , (int)( J ) ) << 24) | (__caller__.Byte( S , (int)( (J + 1) ) ) << 16)) | (__caller__.Byte( S , (int)( (J + 2) ) ) << 8)) | __caller__.Byte( S , (int)( (J + 3) ) )) ) ; 
                /*700*/ } 
            
            /*705*/ NAVCIPHER (  __context__ , __caller__ ,   ref  DECKEY , (uint[,])( DECMIX ),   ref  DECS ,   ref  BLOCK ) ; 
            /*707*/ ushort __FN_FORSTART_VAL__2 = (ushort) ( 0 ) ;
            ushort __FN_FOREND_VAL__2 = (ushort)3; 
            int __FN_FORSTEP_VAL__2 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (I  >= __FN_FORSTART_VAL__2) && (I  <= __FN_FOREND_VAL__2) ) : ( (I  <= __FN_FORSTART_VAL__2) && (I  >= __FN_FOREND_VAL__2) ) ; I  += (ushort)__FN_FORSTEP_VAL__2) 
                { 
                /*708*/ J = (ushort) ( (3 & Functions.ToInteger( -( I ) )) ) ; 
                /*709*/ __caller__.SetByte ( A ,  (ushort) ( 1 ) ,  (ushort) ( Functions.Low( (ushort) (BLOCK[ J ] >> 24) ) ) ) ; 
                /*710*/ __caller__.SetByte ( A ,  (ushort) ( 2 ) ,  (ushort) ( Functions.Low( (ushort) (BLOCK[ J ] >> 16) ) ) ) ; 
                /*711*/ __caller__.SetByte ( A ,  (ushort) ( 3 ) ,  (ushort) ( Functions.Low( (ushort) (BLOCK[ J ] >> 8) ) ) ) ; 
                /*712*/ __caller__.SetByte ( A ,  (ushort) ( 4 ) ,  (ushort) ( Functions.Low( (ushort) BLOCK[ J ] ) ) ) ; 
                /*714*/ __caller__.SetString ( A , (int)((I * 4) + 1), P ) ; 
                /*707*/ } 
            
            /*717*/ return ( P ) ; 
            
            }
            
        static public CrestronString NAV_AES128_ENCRYPT (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SSTRINGTOENCRYPT , CrestronString SKEY ) 
            { 
            CrestronString IN;
            CrestronString OUT;
            CrestronString TMP1;
            CrestronString TMP2;
            IN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, __caller__ );
            OUT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, __caller__ );
            TMP1  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16, __caller__ );
            TMP2  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16, __caller__ );
            
            ushort I = 0;
            ushort L = 0;
            ushort KEYGOOD = 0;
            ushort [] ENCS;
            ushort [] DECS;
            ENCS  = new ushort[ 256 ];
            DECS  = new ushort[ 256 ];
            
            uint [] B;
            uint [] ENCKEY;
            uint [] DECKEY;
            uint [,] ENCMIX;
            uint [,] DECMIX;
            B  = new uint[ 17 ];
            ENCKEY  = new uint[ 44 ];
            DECKEY  = new uint[ 44 ];
            ENCMIX  = new uint[ 4,256 ];
            DECMIX  = new uint[ 4,256 ];
            
            
            /*726*/ NAVCONSTANTS (  __context__ , __caller__ ,   ref  ENCS ,   ref  DECS , (uint[,])( ENCMIX ), (uint[,])( DECMIX )) ; 
            /*727*/ ENCKEY [ 0] = (uint) ( 0 ) ; 
            /*728*/ ENCKEY [ 1] = (uint) ( 0 ) ; 
            /*729*/ ENCKEY [ 2] = (uint) ( 0 ) ; 
            /*730*/ ENCKEY [ 3] = (uint) ( 0 ) ; 
            /*731*/ NAVSETKEY (  __context__ , __caller__ , SKEY,   ref  ENCKEY ,   ref  KEYGOOD ) ; 
            /*732*/ NAVKEYSCHEDULE (  __context__ , __caller__ ,   ref  ENCS , (uint[,])( DECMIX ),   ref  ENCKEY ,   ref  DECKEY ,   ref  KEYGOOD ) ; 
            /*734*/ IN  .UpdateValue ( SSTRINGTOENCRYPT  ) ; 
            /*736*/ while ( Functions.TestForTrue  ( ( Functions.BoolToInt (KEYGOOD == 0))  ) ) 
                { 
                /*736*/ Functions.Delay (  (int) ( 1 ) ) ; 
                /*736*/ } 
            
            /*737*/ L = (ushort) ( Functions.Length( IN ) ) ; 
            /*738*/ I = (ushort) ( ((16 - L) & 15) ) ; 
            /*739*/ while ( Functions.TestForTrue  ( ( (L & 15))  ) ) 
                { 
                /*740*/ __caller__.SetString ( Functions.Chr (  (int) ( I ) ) , (int)(L + 1), IN ) ; 
                /*741*/ L = (ushort) ( Functions.Length( IN ) ) ; 
                /*739*/ } 
            
            /*744*/ I = (ushort) ( 1 ) ; 
            /*745*/ while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( I < L ))  ) ) 
                { 
                /*746*/ TMP1  .UpdateValue ( Functions.Mid ( IN ,  (int) ( I ) ,  (int) ( 16 ) )  ) ; 
                /*747*/ TMP2  .UpdateValue ( NAVENCRYPT (  __context__ , __caller__ , TMP1,   ref  ENCKEY , (uint[,])( ENCMIX ),   ref  ENCS )  ) ; 
                /*748*/ __caller__.SetString ( TMP2 , (int)I, OUT ) ; 
                /*749*/ I = (ushort) ( (I + 16) ) ; 
                /*745*/ } 
            
            /*753*/ return ( OUT ) ; 
            
            }
            
        static public CrestronString NAV_AES128_DECRYPT (  SplusExecutionContext __context__, SplusObject __caller__ , CrestronString SSTRINGTODECRYPT , CrestronString SKEY ) 
            { 
            CrestronString IN;
            CrestronString OUT;
            CrestronString TMP1;
            CrestronString TMP2;
            IN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, __caller__ );
            OUT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, __caller__ );
            TMP1  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16, __caller__ );
            TMP2  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16, __caller__ );
            
            ushort I = 0;
            ushort L = 0;
            ushort KEYGOOD = 0;
            ushort [] ENCS;
            ushort [] DECS;
            ENCS  = new ushort[ 256 ];
            DECS  = new ushort[ 256 ];
            
            uint [] B;
            uint [] ENCKEY;
            uint [] DECKEY;
            uint [,] ENCMIX;
            uint [,] DECMIX;
            B  = new uint[ 17 ];
            ENCKEY  = new uint[ 44 ];
            DECKEY  = new uint[ 44 ];
            ENCMIX  = new uint[ 4,256 ];
            DECMIX  = new uint[ 4,256 ];
            
            
            /*762*/ NAVCONSTANTS (  __context__ , __caller__ ,   ref  ENCS ,   ref  DECS , (uint[,])( ENCMIX ), (uint[,])( DECMIX )) ; 
            /*763*/ ENCKEY [ 0] = (uint) ( 0 ) ; 
            /*764*/ ENCKEY [ 1] = (uint) ( 0 ) ; 
            /*765*/ ENCKEY [ 2] = (uint) ( 0 ) ; 
            /*766*/ ENCKEY [ 3] = (uint) ( 0 ) ; 
            /*767*/ NAVSETKEY (  __context__ , __caller__ , SKEY,   ref  ENCKEY ,   ref  KEYGOOD ) ; 
            /*768*/ NAVKEYSCHEDULE (  __context__ , __caller__ ,   ref  ENCS , (uint[,])( DECMIX ),   ref  ENCKEY ,   ref  DECKEY ,   ref  KEYGOOD ) ; 
            /*770*/ IN  .UpdateValue ( SSTRINGTODECRYPT  ) ; 
            /*771*/ while ( Functions.TestForTrue  ( ( Functions.BoolToInt (KEYGOOD == 0))  ) ) 
                { 
                /*771*/ Functions.Delay (  (int) ( 1 ) ) ; 
                /*771*/ } 
            
            /*772*/ I = (ushort) ( 1 ) ; 
            /*772*/ L = (ushort) ( Functions.Length( IN ) ) ; 
            /*773*/ while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( I < L ))  ) ) 
                { 
                /*774*/ TMP1  .UpdateValue ( Functions.Mid ( IN ,  (int) ( I ) ,  (int) ( 16 ) )  ) ; 
                /*775*/ TMP2  .UpdateValue ( NAVDECRYPT (  __context__ , __caller__ , TMP1,   ref  DECKEY , (uint[,])( DECMIX ),   ref  DECS )  ) ; 
                /*776*/ __caller__.SetString ( TMP2 , (int)I, OUT ) ; 
                /*777*/ I = (ushort) ( (I + 16) ) ; 
                /*773*/ } 
            
            /*781*/ I = (ushort) ( __caller__.Byte( OUT , (int)( Functions.Length( OUT ) ) ) ) ; 
            /*782*/ return ( Functions.Left ( OUT ,  (int) ( (Functions.Length( OUT ) - I) ) ) ) ; 
            
            }
            
        
        public UserModuleClass_NAVFOUNDATION ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
        
        public class __CEvent__NAVFOUNDATION : CEvent
        {
            public __CEvent__NAVFOUNDATION() {}
            public void Close() { base.Close(); }
            public int Reset() { return base.Reset() ? 1 : 0; }
            public int Set() { return base.Set() ? 1 : 0; }
            public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
        }
        public class __CMutex__NAVFOUNDATION : CMutex
        {
            public __CMutex__NAVFOUNDATION() {}
            public void Close() { base.Close(); }
            public void ReleaseMutex() { base.ReleaseMutex(); }
            public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
        }
        static public int IsNullNAVFOUNDATION( object obj ){ return (obj == null) ? 1 : 0; }
    }
    
    [SplusStructAttribute(-1, true, false)]
    public class _NAVRANGE : SplusStructureBase
    {
    
        [SplusStructAttribute(0, false, false)]
        public ushort  ILOCATION = 0;
        
        [SplusStructAttribute(1, false, false)]
        public ushort  IDISTANCE = 0;
        
        
        public _NAVRANGE( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
        {
            
            
        }
        
    }
    [SplusStructAttribute(-1, true, false)]
    public class _NAVPOINT : SplusStructureBase
    {
    
        [SplusStructAttribute(0, false, false)]
        public ushort  X = 0;
        
        [SplusStructAttribute(1, false, false)]
        public ushort  Y = 0;
        
        
        public _NAVPOINT( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
        {
            
            
        }
        
    }
    [SplusStructAttribute(-1, true, false)]
    public class _NAVSIZE : SplusStructureBase
    {
    
        [SplusStructAttribute(0, false, false)]
        public ushort  IWIDTH = 0;
        
        [SplusStructAttribute(1, false, false)]
        public ushort  IHEIGHT = 0;
        
        
        public _NAVSIZE( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
        {
            
            
        }
        
    }
    [SplusStructAttribute(-1, true, false)]
    public class _NAVIPCONNECTION : SplusStructureBase
    {
    
        [SplusStructAttribute(0, false, false)]
        public CrestronString  SADDRESS;
        
        [SplusStructAttribute(1, false, false)]
        public ushort  IPORT = 0;
        
        [SplusStructAttribute(2, false, false)]
        public ushort  ICONNECTED = 0;
        
        [SplusStructAttribute(3, false, false)]
        public ushort  IAUTHENTICATED = 0;
        
        
        public _NAVIPCONNECTION( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
        {
            SADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
            
            
        }
        
    }
    [SplusStructAttribute(-1, true, false)]
    public class _NAVPHONEBOOKENTRY : SplusStructureBase
    {
    
        [SplusStructAttribute(0, false, false)]
        public CrestronString  SNAME;
        
        [SplusStructAttribute(1, false, false)]
        public CrestronString  SNUMBER;
        
        
        public _NAVPHONEBOOKENTRY( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
        {
            SNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, Owner );
            SNUMBER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, Owner );
            
            
        }
        
    }
    [SplusStructAttribute(-1, true, false)]
    public class _NAVLEVEL : SplusStructureBase
    {
    
        [SplusStructAttribute(0, false, false)]
        public short  SIREQUIREDLEVEL = 0;
        
        [SplusStructAttribute(1, false, false)]
        public short  SIACTUALLEVEL = 0;
        
        
        public _NAVLEVEL( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
        {
            
            
        }
        
    }
    [SplusStructAttribute(-1, true, false)]
    public class _NAVSTATE : SplusStructureBase
    {
    
        [SplusStructAttribute(0, false, false)]
        public ushort  SIREQUIREDSTATE = 0;
        
        [SplusStructAttribute(1, false, false)]
        public ushort  SIACTUALSTATE = 0;
        
        
        public _NAVSTATE( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
        {
            
            
        }
        
    }
    [SplusStructAttribute(-1, true, false)]
    public class _NAVDEVICE : SplusStructureBase
    {
    
        [SplusStructAttribute(0, false, false)]
        public ushort  IONLINE = 0;
        
        [SplusStructAttribute(1, false, false)]
        public ushort  ICOMMUNICATING = 0;
        
        [SplusStructAttribute(2, false, false)]
        public ushort  IINITIALIZED = 0;
        
        
        public _NAVDEVICE( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
        {
            
            
        }
        
    }
    [SplusStructAttribute(-1, true, false)]
    public class _NAVMODULE : SplusStructureBase
    {
    
        [SplusStructAttribute(0, false, false)]
        public CrestronString  SRXBUFFER;
        
        [SplusStructAttribute(1, false, false)]
        public ushort  ISEMAPHORE = 0;
        
        [SplusStructAttribute(2, false, false)]
        public ushort  IENABLED = 0;
        
        [SplusStructAttribute(3, false, false)]
        public ushort  ICOMMANDBUSY = 0;
        
        
        public _NAVMODULE( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
        {
            SRXBUFFER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1024, Owner );
            
            
        }
        
    }
    
}
