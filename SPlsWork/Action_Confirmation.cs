using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;
using UserModule_NAVFOUNDATION;

namespace UserModule_ACTION_CONFIRMATION
{
    public class UserModuleClass_ACTION_CONFIRMATION : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput INITIALIZE;
        Crestron.Logos.SplusObjects.DigitalInput CONFIRM;
        Crestron.Logos.SplusObjects.DigitalInput CANCEL;
        Crestron.Logos.SplusObjects.DigitalOutput WARNING_FB;
        Crestron.Logos.SplusObjects.DigitalOutput CONFIRMED_FB;
        Crestron.Logos.SplusObjects.DigitalOutput CANCELLED_FB;
        UShortParameter PULSE_TIME;
        ushort IWARNING = 0;
        private void RESETVALUES (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 173;
            IWARNING = (ushort) ( 0 ) ; 
            
            }
            
        private void FEEDBACK (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 177;
            WARNING_FB  .Value = (ushort) ( IWARNING ) ; 
            
            }
            
        object INITIALIZE_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 185;
                IWARNING = (ushort) ( 1 ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object CONFIRM_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 189;
            if ( Functions.TestForTrue  ( ( IWARNING)  ) ) 
                { 
                __context__.SourceCodeLine = 190;
                Functions.Pulse ( PULSE_TIME  .Value, CONFIRMED_FB ) ; 
                __context__.SourceCodeLine = 191;
                IWARNING = (ushort) ( 0 ) ; 
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object CANCEL_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 196;
        if ( Functions.TestForTrue  ( ( IWARNING)  ) ) 
            { 
            __context__.SourceCodeLine = 197;
            Functions.Pulse ( PULSE_TIME  .Value, CANCELLED_FB ) ; 
            __context__.SourceCodeLine = 198;
            IWARNING = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 263;
        WaitForInitializationComplete ( ) ; 
        __context__.SourceCodeLine = 264;
        RESETVALUES (  __context__  ) ; 
        __context__.SourceCodeLine = 265;
        while ( Functions.TestForTrue  ( ( 1)  ) ) 
            { 
            __context__.SourceCodeLine = 266;
            Functions.Delay (  (int) ( 20 ) ) ; 
            __context__.SourceCodeLine = 267;
            FEEDBACK (  __context__  ) ; 
            __context__.SourceCodeLine = 265;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    
    INITIALIZE = new Crestron.Logos.SplusObjects.DigitalInput( INITIALIZE__DigitalInput__, this );
    m_DigitalInputList.Add( INITIALIZE__DigitalInput__, INITIALIZE );
    
    CONFIRM = new Crestron.Logos.SplusObjects.DigitalInput( CONFIRM__DigitalInput__, this );
    m_DigitalInputList.Add( CONFIRM__DigitalInput__, CONFIRM );
    
    CANCEL = new Crestron.Logos.SplusObjects.DigitalInput( CANCEL__DigitalInput__, this );
    m_DigitalInputList.Add( CANCEL__DigitalInput__, CANCEL );
    
    WARNING_FB = new Crestron.Logos.SplusObjects.DigitalOutput( WARNING_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( WARNING_FB__DigitalOutput__, WARNING_FB );
    
    CONFIRMED_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONFIRMED_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONFIRMED_FB__DigitalOutput__, CONFIRMED_FB );
    
    CANCELLED_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CANCELLED_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CANCELLED_FB__DigitalOutput__, CANCELLED_FB );
    
    PULSE_TIME = new UShortParameter( PULSE_TIME__Parameter__, this );
    m_ParameterList.Add( PULSE_TIME__Parameter__, PULSE_TIME );
    
    
    INITIALIZE.OnDigitalPush.Add( new InputChangeHandlerWrapper( INITIALIZE_OnPush_0, false ) );
    CONFIRM.OnDigitalPush.Add( new InputChangeHandlerWrapper( CONFIRM_OnPush_1, false ) );
    CANCEL.OnDigitalPush.Add( new InputChangeHandlerWrapper( CANCEL_OnPush_2, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_ACTION_CONFIRMATION ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint INITIALIZE__DigitalInput__ = 0;
const uint CONFIRM__DigitalInput__ = 1;
const uint CANCEL__DigitalInput__ = 2;
const uint WARNING_FB__DigitalOutput__ = 0;
const uint CONFIRMED_FB__DigitalOutput__ = 1;
const uint CANCELLED_FB__DigitalOutput__ = 2;
const uint PULSE_TIME__Parameter__ = 10;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
